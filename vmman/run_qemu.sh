#!/bin/bash
#XXX: rewrite
source ./config.sh

#XXX: trap

function scream() {
	local VM_NAME=${1};
	local VM_OPTS=${@:2};

	local VM_CONF=(${VMS[${VM_NAME}]});
	#TODO: verify VM_CONF is AT LEAST 3 long

	local VM_UID="${VM_CONF[0]}";
	local VM_GID="${VM_CONF[1]}";
	local VM_IMG="${VM_CONF[2]}";
	local VM_IFS="${VM_CONF[@]:3}";

	local VM_RUN="/var/run/${VM_UID}";
	local VM_MON="unix:${VM_RUN}/${VM_NAME}_mon.sock";
	local VM_PID="${VM_RUN}/${VM_NAME}.pid";

	./qemu_netman.bash initialize ${VM_NAME};
	local RV=$?
	if [ ${RV} -ne 0 ]; then
		>&2 echo "qemu_netman returned ${RV}. If this is unexpected," \
		"please check your processes and try running qemu_netman deinit ${VM_NAME}\n" \
		"Exiting with ${RV}."
		exit ${RV}
	fi;

	local IMGPARAMS="";
	local NETPARAMS="";
	local INDEX=0;

	if [ ${VM_IMG} != 'null' ]; then
		# this is possible with a blockdev, sort of like how networking is
#		IMGPARAMS+="-device virtio-blk-pci,drive=${VM_IMG}";
		IMGPARAMS+="-drive file=${VM_IMG},if=virtio";
	fi;

	for IF in ${VM_IFS[@]}; do
		local IF_CONF=(${IFACES[${IF}]});
		local IF_MAC="${IF_CONF[0]}";
		local DEV_ID="vt${INDEX}";

		NETPARAMS+="-netdev tap,id=${DEV_ID},ifname=${IF},script=./qemu_net_init.sh,downscript=no ";
		echo ${DEINIT_EXE}
		NETPARAMS+="-device virtio-net-pci,mac=${IF_MAC},netdev=${DEV_ID}";
		INDEX=$(( ++INDEX ));
	done;

	sudo qemu-system-x86_64 \
		-name ${VM_NAME} \
		-monitor ${VM_MON},server,nowait \
		-runas ${VM_UID} -pidfile ${VM_PID} \
		-cpu host -enable-kvm -machine pc,accel=kvm \
		${NETPARAMS} \
		${IMGPARAMS} \
		${VM_OPTS}

	>&2 echo "INFO: QEMU returned ${RV}, attempting to deinitialize ${VM_NAME} networking."
	./qemu_netman.bash deinitialize ${VM_NAME}


	
#	declare -a TAPS=()
	
#	for IFACE in ${HOSTIFS}; do
#		read -a IFLIST <<< ${IFACES[${IFACE}]}
#		TAP=${IFLIST[1]}
#		TAPS+=(${TAP})
#	#	NETPARAMS+="-net tap,ifname=${TAP},vlan=${INDEX},script=./qemu_net_init.sh,downscript=no -net nic,model=virtio,vlan=${INDEX},macaddr=52:54:98:90:31:C7 "
#		INDEX=$(( ++INDEX ))
#	done
#	echo ${NETPARAMS}
		
	
#	qemu-system-x86_64                                                     \
#		-monitor unix:/var/run/${VM_UID}/${VM_NAME}_mon.sock,server,nowait \
	
	#	-m 4G                                                          \
#		${NETPARAMS}                                                   \
	#	-smp 2                                                         \
	#	-cpu host -enable-kvm -machine pc,accel=kvm                    \
	#	-runas fedora-vm -daemonize -pidfile /var/run/fedora-vm/debian_desktop.pid    \
	#	debian.qcow2                                                   \
	#	$@
	
	#	-vnc :3                                                        \
	#	-vnc :2                                                        \
	#	-serial mon:stdio                                              \
	#	-net nic,model=virtio                                          \
	#	-net tap,ifname=${TAP},script=./qemu_net_init.sh,downscript=no \
	#	-net nic,model=virtio
	#	-serial mon:stdio                                              \
	#	-nographic                                                     \
	#	-kernel ./builds/bzImage                                       \
	#	-initrd ./builds/initramfs.cpio.gz                             \
	#	-append "init=/bin/init console=ttyS0,9600n8 loglevel=0"
	

}

VM_NAME=${1};

scream ${VM_NAME} ${@:2};
