# qemu net setup config


# vmman Configuration
# ```````````````````
# IFACES is an associative array of interfaces to be used by 
# qemu_net_setup.sh's init(), which is called by run_qemu.sh.
#
# VMS is an associative array of virtual machines with information
# on which IFACE rules to use, where to find the VM's files, and which
# user it runs under.
# 
# See APPENDIX A for information on QEMU's networking.
#
# The syntax for configuration is:
#
# IFACES[tap]='vm_MAC physical_device bridge prefix route'
# VMS[vm_name]='user group /path/to/hdd.img /path/to/pid.pid [tapX ...]' 
# 
# XXX NOTE: IP addressing is not implemented yet. XXX
# * VMS[vm]'s tap devices are optional and may have infinitely many taps.
# * The IFACES[tapX] is optional if there is no tap in the VMS keyval.
# * The IFACES[tapX]'s physical device is optional.
#     - If the physical device is not specified, prefix and route SHOULD be 'null'.
#     - If the physical device is specified, prefix and route are optional and 
#       MAY be 'null' for interfaces the host does not need networking on.
# * The IFACES[tapX]'s prefix MAY be:
#     - an address in CIDR notation
#     - 'dhcp' for dhcp
#     - 'null' for nothing
#     - blank (but SHOULD not be)
# * The IFACES[tapX]'s route DEPENDS on the prefix, respectively:
#     - an addresss
#     - 'null' for dhcp or nothing
#     - blank (but SHOULD not be)
#
# Also see APPENDIX B.

#  APPENDIX A, or:
# On QEMU Networking
# ``````````````````
# QEMU can use taps and bridges in a "multi-tap single-bridge" mode.
# Meaning, there is one bridge per physical device, and multiple taps
# for each virtual machine, e.g. eth0 <-> br0 <-> {tap00, tap01}.
#
# Here's an ascii diagram representing three virtual machines with five
# virtual NICs utilizing five taps and two bridges:
#
#                                              \
#               ,- tap00 -- [VM Foo] -- tap10 -.
#              |                     XX         |
# eth0 -- br0 -+-- tap01 -- [VM Bar] XX          > br1 -- eth1
#              |                     XX         |
#               `- tap02 -- [VM Bar] -- tap11 -'
#
# XXX: what about inter-VM networking?

# APPENDIX B
# ``````````
# - vm_MAC is the physical address used by the NIC inside QEMU,
#   not the host's tap. if you need this feature, request it, and please let
#   us know what it's useful for!
# - The prefix and route correspond to the host's address, not the guest's!
# - In light of recent x86_64 vulnerability disclosures, keep in mind which
#   NICs are being assigned to your VMs, especially in use as a router VM;
#   plugging in an interface with access to Intel ME to the public is probably
#   dangerous.
# - Re-using physical_device IS possible, but creating multiple bridges is
#   known to be problematic.
# - PID path is synthesized from user and VM name; e.g. /var/run/user/vm.pid
# - QEMU monitor is similar; e.g. /var/run/user/vm_mon.sock

declare -A VMS IFACES

# Example configuration for a first VM to start, such as a router.
# By specifying the physical device and address/route, qemu_netman acts as the network manager.
# IFACES[tapX]='12:34:56:78:9A:BC ethX brX0 10.10.10.10/24 10.10.10.0'
# IFACES[tapY]='12:34:56:78:9A:BD ethY brY0 192.168.0.2/24 192.168.0.1'
# VMS[first_vm]='username usergroup /path/to/vm/image.img tapX tapY'

# Example configuration for an auxiliary VM to start, such as a services VM.
# By specifying 'null' for the physical device and address/route, the OS manages the bridge.
# IFACES[tapZ]='12:34:56:78:9A:BE null brZ0 null null'
# VMS[aux_vm]='aux_user aux_group /path/to/aux/vm_image.img tapZ'

# Example configuration for a VM with no networking, such as a sandbox
# VMS[sandbox]='sandbox_user sandbox_group /path/to/sandbox.img'

IFACES[tapLAN0]='52:54:00:F1:54:86 eno1 brLAN0 10.69.0.2/16 10.69.0.1'
IFACES[tapWAN0]='52:54:00:F1:54:87 enp2s0 brWAN0 192.168.100.2/24 none'
VMS[pfsense-vm]='pfsense-vm pfsense-vm /opt/pfsense-vm/pfsense.qcow2 tapLAN0 tapWAN0'

IFACES[tapLAN1]='52:54:00:90:31:C7 null brLAN0 none none'
VMS[tinycore]='kbjensen kbjensen /home/kbjensen/prog/misc/vmman/tinycore.qcow2 tapLAN1'


IFACES[tapDUM0]='12:34:56:78:9A:BC dum0 brDUM0 192.168.254.254/24 192.168.254.1'
IFACES[tapDUM1]='12:34:56:78:9A:BD dum1 brDUM1 192.168.1.2/24 192.168.1.1'
VMS[test-vm]='kbjensen kbjensen /dev/null tapDUM0 tapDUM1'
TAPIFS=${!IFACES[@]} # keys of IFACES
VMNAMES=${!VMS[@]}   # keys of VMS
