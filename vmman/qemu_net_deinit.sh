#!/usr/bin/env bash
# pretty straightforward helper script

VM_NAME=${0#*^}
IF=${1}


if [[ ${0} =~ '.sh' ]]; then
	>&2 echo "ERR: cannot remove original ${VM_NAME} deinitialization executable! Exiting with 128."
	exit 128;
fi;

>&2 echo "INFO: VM_NAME ${VM_NAME}."
>&2 echo "INFO: deinitializing ${VM_NAME} networking."
echo $(whoami) &> /var/run/kbjensen/out
/home/kbjensen/prog/misc/vmman/qemu_netman.bash deinitialize ${VM_NAME} &>> /var/run/kbjensen/out
RV=${?}

if ! [[ ${0} =~ '.sh' ]]; then
	rm -v ${0}
fi;


exit ${RV}
