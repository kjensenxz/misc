#!/usr/bin/env bash
# pretty straightforward helper script

IF=${1}

>&2 echo "INFO: setting link for ${IF} up."
sudo ip link set dev ${IF} up

exit ${?}
