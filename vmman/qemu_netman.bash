#!/usr/bin/env bash
# qemu_netman - stateless management of network interfaces for QEMU
# Copyright (C) 2019 Kenneth B. Jensen <kj (at) 0x5f3759df.xyz>
#
# XXX NOTE: qemu_netman does not clean up after itself on initialization   XXX
# XXX       failure; run deinit and please familiarize yourself with:      XXX
# XXX         - bash(1)                                                    XXX
# XXX         - `ip(8)` (ip-addr(8), ip-link(8), ip tuntap help)           XXX
# XXX         - your dhcp client (probably dhcpcd(8) or dhclient(8))       XXX
# XXX         - and some with how taps and bridges work.                   XXX
# 
# Hope you ate your bashisms today!
# TODO: optional management of IP addresses?
source /home/kbjensen/prog/misc/vmman/config.sh;

#################################### User-interface functions ##########
function err() {
	>&2 echo -e "${@}";
}

function die() {
	err "${@:2}";
	exit ${1};
}

function usage() {
	die 1 "Usage: ${1} {init[ialize]|de[initialize]} vm_name\n"
	      "init creates necessary tap and bridge devices.\n"
	      "deinit destroys them.\n"
	      "vm_name is the key of the \`VMS\` array, see config.sh.\n"
	      'note that this script requires escalation.';
}

trap "die 1 'Recieved signal, exiting with 1.'" SIGINT SIGTERM

########################################### Tooling functions ##########
# $1: interface to check
# returns 0 on success, non-zero for error
# refer to ip(8) for errors.
function iface_exists {
	local IF=${1};

	ip link show ${IF} &> /dev/null;

	local RV=${?};
	return ${RV};
}

# $1: interface to check
# $2: master (bridge) to check
# returns 0, 1, or dies
# 0: MASTER = BR, no change necessary
# 1: No MASTER, modify IF
function check_ifmaster {
	local IF=${1};
	local BR=${2};
	
	# do a lot of sanity checking
	if [[ -z ${IF} || -z ${BR} ]]; then
		die 1 "ERR: Could not check master!\n" \
		      "DEBUG: Interface: '${IF}' Bridge: '${BR}'" \
		      'Please check your configuration. Exiting with 1.'
	fi;

	# IFSTAT is reused later for determining MASTER...
	local IFSTAT=$(ip link show ${IF});
	local RV=${?};
	if [ ${RV} -ne 0 ]; then
		die 1 "ERR: '${IF}' does not exist!\n" \
		      'Please check your configuration. Exiting with 1.'
	fi;

	# ...otherwise this could be in a for loop! 
	if ! iface_exists ${BR}; then
		die 1 "ERR: '${BR}' does not exist!\n" \
		      'Please check your configuration. Exiting with 1.'
	fi;

	local MASTER=$(                           
		egrep --only-matching 'master [A-Za-z0-9]+' <<< ${IFSTAT} | \
		cut --delimiter=' ' --fields=2
	);

	# length of $MASTER will be zero if there is no master of interface
	if [ -z ${MASTER} ]; then
		return 1;
	elif [ ${MASTER} = ${BR} ]; then
		return 0;
	fi;
	# something is misconfigured!
	die 1 "ERR: Master of '${IF}' is '${MASTER}', expected '${BR}'!\n" \
	      'Please check your system and configuration. Exiting with 1.';
}

# Check if interface is up or not
# $1: Interface to check state of
# Returns 0 if up, 1 otherwise, dies if a bad interface is up.
function is_ifup() {
	local IF=${1};

	if ! iface_exists ${IF}; then
		die 1 "ERR: Could not check status of interface '${IF}.'\n" \
		      'Please check your configuration, PATH, and permissions.' \
		      'Exiting with 1.';
	fi;

	local IFSTAT=$(
		ip link show ${IF} | \
		egrep --only-matching 'state [A-Z]+' | \
		cut --delimiter=' ' --field=2
	)
	
	# returns 0 (true) or 1 (false)
	[[ ${IFSTAT} = 'UP' ]]
}

############################################ Worker Functions ##########

# Create bridge device, if not already created, 
# and add its slaves if not already slaved.
# $1: Bridge to create (will reuse if exists)
# $@: Bridge slaves (will check if 'null')
function create_bridge() {
	local BR=${1}

	# create bridge before adding its slaves
	if iface_exists ${BR}; then
		err "WARN: Bridge '${BR}' exists, reusing it.";
	else
		sudo ip link add ${BR} type bridge;
		local RV=${?};
		if [ ${RV} -ne 0 ]; then
			die 1 "ERR: Could not create bridge '${BR}'.\n" \
			      'Please check your permissions. Exiting with 1.';
		fi;
	fi;

	# Set bridge link to up, even if it already exists
	if ! is_ifup ${BR}; then 
		sudo ip link set dev ${BR} up;
		local RV=${?};
		if [ ${RV} -ne 0 ]; then
			die 1 "ERR: Could not set bridge '${BR}' link to UP.\n"
			      "Please check your permissions."
			      "Exiting with 1.";
		fi;
	fi;

	# Add bridge slaves
	for IF in "${@:2}"; do
		# Sanity check!
		if [ ${IF} = 'null' ]; then
			continue;
		elif ! iface_exists ${IF}; then
			die 1 "ERR: Interface '${IF}' does not exist.\n" \
			      'Please check your configuration and network interfaces.' \
			      'Exiting with 1.';
		elif check_ifmaster ${IF} ${BR}; then
			err "INFO: Interface '${IF}' is already bridged to '${BR}.'" 
			    'Continuing.'
			continue;
		fi;

		err "INFO: Adding '${IF}' as slave to '${BR}.'"
		sudo ip link set dev ${IF} master ${BR};
		local RV=${?};
		if [ ${RV} -ne 0 ]; then
			die 1 "ERR: Could not bridge '${IF}' to '${BR}.'\n"\
			      'Please check your permissions. Exiting with 1.';
		fi
	done;

	#TODO: Move IPs
}

# Remove bridge slaves and destroy bridge device, if exists.
# Makes a best-effort to clean-up.
# $1: Bridge to destroy
# $@: Bridge slaves (will check if 'null')
function delete_bridge() {
	local BR=${1}

	if ! iface_exists ${BR}; then
		err "WARN: Bridge '${BR}' doesn't exist, doing nothing.";
		return;
	fi;

	# Remove bridge slaves
	for IF in "${@:2}"; do
		# Just for error-reporting
		if [ ${IF} = 'null' ]; then
			:;
		elif ! iface_exists ${IF}; then
			err "ERR: Interface '${IF}' does not exist.\n" \
			    'Please check your configuration and network interfaces.' \
			    'Continuing with errors.';
		elif check_ifmaster ${IF} ${BR}; then
			err "INFO: Removing interface '${IF}' from bridge '${BR}.'";
			sudo ip link set dev ${IF} nomaster;
			local RV=${?};
			if [ ${RV} -ne 0 ]; then
				err "ERR: Could not unbridge '${IF}' from '${BR}.'\n" \
				    'Please check your interfaces and permissions.' \
				    'Continuing with errors.';
			fi;
		fi;

	done;

	sudo ip link del ${BR};
	local RV=${?};
	if [ ${RV} -ne 0 ]; then
		err "ERR: Could not delete bridge '${BR}'.\n" \
		    'Please check your permissions.' \
		    'Continuing with errors.';
	fi;

	#TODO: Move IPs
}

# $1: UID to use
# $2: GID to use
# $@: Taps to create
function create_taps() {
	local _UID=${1};
	local _GID=${2};

	
	if [ ${#@} -lt 3 ]; then
		die 1 "ERR: Not enough parameters to create tap devices.\n" \
		      "PARAMS: '${@}' (#: ${#@})\n" \
		      'Please check your configuration. Exiting with 1.';
	fi;

	for TAP in "${@:3}"; do
		if iface_exists ${TAP}; then
			die 1 "ERR: Tap '${TAP}' already exists and may be in use.\n" \
			      'Please check your configuration, running services,' \
			      'and network interfaces. Exiting with 1.';
		fi;

		err "INFO: Creating tap '${TAP}' for UID '${_UID}'/GID '${_GID}.'";
		sudo ip tuntap add dev ${TAP} mode tap user ${_UID} group ${_GID};
		local RV=${?}
		if [ ${RV} -ne 0 ]; then
			die 1 "ERR: ip tuntap returned '${RV}.'\n" \
			'Please check your modules, permissions, and users/groups.'
			' Exiting with 1.';
		fi;

		# note that a tap's link cannot be set to up without a process
		# attached to it (e.g. qemu). 
	done;
}

# Makes a best-effort to clean up.
# $@: Taps to delete
function delete_taps() {
	for TAP in "${@}"; do
		if ! iface_exists ${TAP}; then
			err "ERR: Tap '${TAP}' does not exist, doing nothing.\n" \
			    'Continuing with errors.';
			continue;
		fi;

		err "INFO: Deleting tap '${TAP}.'";
		sudo ip tuntap del dev ${TAP} mode tap;
		local RV=${?}
		if [ ${RV} -ne 0 ]; then
			err "ERR: ip tuntap returned '${RV}.'\n" \
			'Please check your modules, permissions, and interfaces.' \ 
			'Continuing with errors.';
		fi;
	done;
}

######################################### Interface functions ##########

# set-up interfaces listed in VMS associative array by key
# - parse configuration
# - create taps
# - create bridge
# - sanity check all along the way
function init_vm_net() {
	local VM_NAME=${1};

	# all we really need from VMS is the UID, GID, and interfaces
	local VMS_VAL=(${VMS[${VM_NAME}]});
	local VM_UID='';
	local VM_GID='';
	local VM_IFS='';

	# derived from IFACES[VM_IFS[n]]
	local TAP_PHY='';
	local TAP_BRD='';

	if [ "${#VMS_VAL[@]}" -eq 0 ]; then
		die 1 "Invalid or zero-length key '${VM_NAME}'.\n" \
		      'Please check your invocation and configuration.' \
		      'Exiting with 1.';
	# only 3 params are required: UID, GID, and IMG; the remaining are optional.
	elif [ $(wc --words <<< ${VMS_VAL[@]}) -le 3 ]; then
		die 0 "No initialization to be done for '${VM_NAME}'.\n" \
		      "If this is not expected, please check your configuration.\n" \
		      'Exiting with 0.';
	fi;

	local VM_UID="${VMS_VAL[0]}"
	local VM_GID="${VMS_VAL[1]}"
	# VMS_VAL[2] is the VM_IMG
	local VM_IFS=(${VMS_VAL[@]:3})

	create_taps "${VM_UID}" "${VM_GID}" ${VM_IFS[@]};
	err "INFO: Successfully created ${VM_IFS}."

	for TAP in ${VM_IFS[@]}; do
		local IF_CONF=(${IFACES[${TAP}]});
		local TAP_PHY="${IF_CONF[1]}"
		local TAP_BRD="${IF_CONF[2]}"
		err "INFO: Attempting to bridge ${TAP_PHY} & ${TAP} to ${TAP_BRD}"

		# bridge the tap interface and the physical interface
		# create_bridge WILL reuse ${TAP_BRD}
		create_bridge ${TAP_BRD} ${TAP} ${TAP_PHY}

		if [ "${TAP_PHY}" != 'null' ] && ! is_ifup "${TAP_PHY}"; then
			err "WARN: Physical device ${TAP_PHY} down.";
			sudo ip link set dev "${TAP_PHY}" up;
			local RV=${?};
			if [ ${RV} -ne 0 ]; then
				err 'ERR: Could not set physical device' \
				'${TAP_PHY} up. Please check your physical' \
				'devices and permissions.' \
				'Continuing with errors.'
			fi;
		fi;
	done;
}

# deconstuct interfaces listed in VMS associative array by key
# this is optional, as the OS may manage your bridges.
function deinit_vm_net() {
	local VM_NAME=${1};

	# all we really need from VMS is the UID, GID, and interfaces
	local VMS_VAL=(${VMS[${VM_NAME}]});
	local VM_IFS='';

	# derived from IFACES[VM_IFS[n]]
	local TAP_PHY='';
	local TAP_BRD='';

	if [ "${#VMS_VAL[@]}" -eq 0 ]; then
		die 1 "Invalid or zero-length key '${VM_NAME}'.\n" \
		      'Please check your invocation and configuration.' \
		      'Exiting with 1.';
	elif [ $(wc --words <<< ${VMS_VAL[@]}) -le 3 ]; then
		die 0 "No deinitialization to be done for '${VM_NAME}'.\n" \
		      "If this is not expected, please check your configuration.\n" \
		      'Exiting with 0.';
	fi;

	local VM_IFS=(${VMS_VAL[@]:3})

	for TAP in ${VM_IFS[@]}; do
		local IF_CONF=(${IFACES[${TAP}]});
		local TAP_PHY="${IF_CONF[1]}";
		local TAP_BRD="${IF_CONF[2]}";
		if [ ${TAP_PHY} != 'null' ]; then
			delete_bridge "${TAP_BRD}" "${TAP}" "${TAP_PHY}";
		fi;
	done;

	delete_taps ${VM_IFS[@]};
}

######################################################## Main ##########

CMD=${1};
VM=${2};

if [ -z ${VM} ]; then
	usage ${0};
fi;

shopt -s extglob
case ${CMD} in
	init?(ialize))
		init_vm_net ${VM};
		;;
	de?(init?(ialize)))
		deinit_vm_net ${VM};
		;;
	*)
		usage ${0};
		;;
esac;
